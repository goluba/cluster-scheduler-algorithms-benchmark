#include <iostream>
#include <iomanip>
#include <memory>

#include <reader.h>
#include <policies/round_robin.h>
#include <policies/srtf.h>
#include <policies/fifo.h>
#include <policies/drf.h>
#include <policies/tetris.h>
#include <policies/slot_based.h>

#include <fstream>

#include <nlohmann/json.hpp>
using json = nlohmann::json;

const size_t NUM_TASKS = 10000;
const size_t NUM_MACHINES = 25;

int main() {
    GoogleReader reader("../data/alibaba.pb", 42);

    const auto& cluster_configuration = reader.GetClusterConfiguration(NUM_MACHINES);
    auto input_tasks = reader.GetSortedTasks(NUM_TASKS);

    std::vector<std::unique_ptr<IScheduler>> schedulers;
    schedulers.push_back(std::make_unique<DRFScheduler>(cluster_configuration));
    schedulers.push_back(std::make_unique<FIFOScheduler>(cluster_configuration));
    schedulers.push_back(std::make_unique<SRTFScheduler>(cluster_configuration));
    schedulers.push_back(std::make_unique<TetrisScheduler>(0, cluster_configuration));
    schedulers.push_back(std::make_unique<TetrisScheduler>(0.25, cluster_configuration));
    schedulers.push_back(std::make_unique<TetrisScheduler>(0.5, cluster_configuration));
    schedulers.push_back(std::make_unique<TetrisScheduler>(0.75, cluster_configuration));
    schedulers.push_back(std::make_unique<SlotBasedScheduler>(cluster_configuration));

    json stats;

    for (auto& scheduler : schedulers) {
        size_t sz = 0;
        std::deque<TaskHolder> tasks;
        for (const auto& task : input_tasks) {tasks.emplace_back(task);
            std::cout << scheduler->GetName() << ": " << ++sz << "/" << input_tasks.size() << " --- " << scheduler->WaitSize() << ";\r";
            fflush(stdout);
            scheduler->Submit(&tasks.back(), task.start_time);
        }
        scheduler->WaitIdle();
        std::cout << "\n";

        int64_t sumWaitTime = 0;
        int64_t sumTaskDurations = 0;
        int64_t executedCount = 0;
        for (const auto& task : tasks) {
            if (task.GetStats().IsExecuted()) {
                executedCount++;
                sumWaitTime += task.GetStats().GetWaitTime() / 100000;

                sumTaskDurations += task.GetDuration() / 100000;
            }
        }
        std::cout << scheduler->GetName() <<  ": ExecutedTasksCount = " << executedCount << "\n";

        stats[scheduler->GetName()]["ExecutedTasksCount"] = executedCount;
        stats[scheduler->GetName()]["AverageWaitTime"] = static_cast<long double>(sumWaitTime) / tasks.size();
        stats[scheduler->GetName()]["AverageTasksDuration"] = static_cast<long double>(sumTaskDurations) / tasks.size();
        stats[scheduler->GetName()]["CpuUtilization"] = scheduler->GetStats().cpu_utilization;
        stats[scheduler->GetName()]["MemoryUtilization"] = scheduler->GetStats().memory_utilization;
        stats[scheduler->GetName()]["DominantResourceUserSatisfactionDeviation"] = scheduler->GetStats().dominant_resource_user_satisfaction_deviation;
        stats[scheduler->GetName()]["DominantResourceUsageDeviation"] = scheduler->GetStats().dominant_resource_usage_deviation;
    }

    std::ofstream file;
    file.open("../statistics/stats.json");
    file << stats.dump();
    file.close();
}
