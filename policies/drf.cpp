#include <policies/drf.h>

#include <algorithm>
#include <unordered_set>

#include <utils.h>

void DRFScheduler::TrySchedule(TTime time) {
    std::priority_queue<std::pair<long double, TUserId>,
                        std::vector<std::pair<long double, TUserId>>,
                        std::greater<>> users_dominant_heap;
    const auto& cluster_capacity = stats_calcer_.cluster_capacity;

    for (const auto& [id, demand] : user_resources_) {
        users_dominant_heap.push({GetUserDominantShare(demand.acquired, cluster_capacity), id});
    }

    std::unordered_map<TUserId, std::unordered_set<size_t>> user_tasks_idxs;
    for (size_t i = 0; i < scheduled_tasks_.size(); i++) {
        // Skip tasks from future
        if (scheduled_tasks_[i]->GetScheduledTime() > time) {
            continue;
        }
        user_tasks_idxs[scheduled_tasks_[i]->GetUserId()].insert(i);
    }

    while (!scheduled_tasks_.empty()) {
        while (!users_dominant_heap.empty() && user_tasks_idxs[users_dominant_heap.top().second].empty()) {
            users_dominant_heap.pop();
        }
        if (users_dominant_heap.empty()) {
            break;
        }
        auto user_id = users_dominant_heap.top().second;

        // Find a task that maximizes Dominant Resource Share for used_id
        auto task_id = *std::max_element(user_tasks_idxs[user_id].begin(), user_tasks_idxs[user_id].end(),
                                           [&](size_t lhs, size_t rhs) {
            const auto& user_acquired = user_resources_[user_id].acquired;
            return GetUserDominantShare(user_acquired + scheduled_tasks_[lhs]->GetRequirements(), cluster_capacity) <
                GetUserDominantShare(user_acquired + scheduled_tasks_[rhs]->GetRequirements(), cluster_capacity);
        });

        auto taskHolder = scheduled_tasks_[task_id];

        bool scheduled = false;
        for (size_t i = 0; i < clusterState_.size(); i++) {
            if (clusterState_[i].TryAcquire(taskHolder->GetRequirements())) {
                ExecuteTask(taskHolder, time, i);


                scheduled_tasks_[task_id] = nullptr;
                users_dominant_heap.pop();
                users_dominant_heap.push({GetUserDominantShare(user_resources_[user_id].acquired, cluster_capacity), user_id});
                user_tasks_idxs[user_id].erase(task_id);

                scheduled = true;
                break;
            }
        }
        if (!scheduled) {
            break;
        }
    }

    auto it = std::remove_if(scheduled_tasks_.begin(), scheduled_tasks_.end(), [](TaskHolder* item) {
        return item == nullptr;
    });
    scheduled_tasks_.erase(it, scheduled_tasks_.end());
}

void DRFScheduler::ReleaseTaskResources(TaskHolder *holder) {
    auto resources = holder->GetRequirements();
    clusterState_[holder->GetMachineId()].Release(resources);
    user_resources_[holder->GetUserId()].acquired -= resources;
    user_resources_[holder->GetUserId()].requested -= resources;
}