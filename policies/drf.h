#pragma once

#include <scheduler.h>

// Dominant Resource Fairness
// https://cs.stanford.edu/~matei/papers/2011/nsdi_drf.pdf
class DRFScheduler : public IScheduler {
public:
    explicit DRFScheduler(const std::vector<Resources>& clusterConfiguration)
            : IScheduler(clusterConfiguration) {
    }

    std::string GetName() const override {
        return "DRF";
    }

protected:
    void TrySchedule(TTime time) override;
    void ReleaseTaskResources(TaskHolder* holder) override;
};