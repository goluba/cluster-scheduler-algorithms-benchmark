#include <policies/fifo.h>
#include <algorithm>

void FIFOScheduler::TrySchedule(TTime time) {
    while (!scheduled_tasks_.empty()) {
        auto taskHolder = scheduled_tasks_.front();

        // Skip tasks from future
        if (taskHolder->GetScheduledTime() > time) {
            return;
        }

        bool scheduled = false;
        for (size_t i = 0; i < clusterState_.size(); i++) {
            if (clusterState_[i].TryAcquire(taskHolder->GetRequirements())) {
                ExecuteTask(taskHolder, time, i);
                scheduled_tasks_.pop_front();
                scheduled = true;
                break;
            }
        }
        if (!scheduled) {
            break;
        }
    }
}

void FIFOScheduler::ReleaseTaskResources(TaskHolder *holder) {
    auto resources = holder->GetRequirements();
    clusterState_[holder->GetMachineId()].Release(resources);
    user_resources_[holder->GetUserId()].acquired -= resources;
    user_resources_[holder->GetUserId()].requested -= resources;
}