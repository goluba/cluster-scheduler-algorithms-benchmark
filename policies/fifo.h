#pragma once

#include <scheduler.h>

// First-in-first-out
class FIFOScheduler : public IScheduler {
public:
    explicit FIFOScheduler(const std::vector<Resources>& clusterConfiguration)
            : IScheduler(clusterConfiguration) {
    }

    std::string GetName() const override {
        return "FIFO";
    }

protected:
    void TrySchedule(TTime time) override;
    void ReleaseTaskResources(TaskHolder* holder) override;
};