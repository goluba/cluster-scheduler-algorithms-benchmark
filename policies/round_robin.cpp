#include <policies/round_robin.h>
#include <algorithm>
#include <vector>
#include <thread>

const size_t NUM_THREADS = 5;

void RoundRobinScheduler::TryScheduleImpl(TTime time, size_t begin, size_t end) {
    for (size_t idx = begin; idx < end; ++idx) {
        auto& taskHolder = scheduled_tasks_[idx];
        // [Optimization]
        if (taskHolder->GetRequirements().cpus > max_cpus_ ||
            taskHolder->GetRequirements().memory > max_memory_) {
            continue;
        }
        // Skip tasks from future
        if (taskHolder->GetScheduledTime() > time) {
            continue;
        }
        for (size_t i = 0; i < clusterState_.size(); i++) {
            if (std::lock_guard lock(mutex_); clusterState_[i].TryAcquire(taskHolder->GetRequirements())) {
                ExecuteTask(taskHolder, time, i);
                taskHolder = nullptr;
                break;
            }
        }
    }
}

void RoundRobinScheduler::TrySchedule(TTime time) {
    for (const auto& machine : clusterState_) {
        max_cpus_ = std::max(max_cpus_, machine.GetIdle().cpus);
        max_memory_ = std::max(max_memory_, machine.GetIdle().memory);
    }

    if (scheduled_tasks_.size() < NUM_THREADS * 10) {
        TryScheduleImpl(time, 0, scheduled_tasks_.size());
    } else {
        std::vector<std::thread> workers;
        for (size_t i = 0; i < scheduled_tasks_.size(); i += scheduled_tasks_.size() / NUM_THREADS) {
            size_t end = std::min(scheduled_tasks_.size(), i + scheduled_tasks_.size() / NUM_THREADS);
            workers.emplace_back([this, begin=i, end, time] {
                TryScheduleImpl(time, begin, end);
            });
        }
        for (auto& worker : workers) {
            worker.join();
        }
    }

    auto it = std::remove_if(scheduled_tasks_.begin(), scheduled_tasks_.end(), [](TaskHolder* item) {
        return item == nullptr;
    });
    scheduled_tasks_.erase(it, scheduled_tasks_.end());
}

void RoundRobinScheduler::ReleaseTaskResources(TaskHolder *holder) {
    auto resources = holder->GetRequirements();
    clusterState_[holder->GetMachineId()].Release(resources);
    user_resources_[holder->GetUserId()].acquired -= resources;
    user_resources_[holder->GetUserId()].requested -= resources;
}