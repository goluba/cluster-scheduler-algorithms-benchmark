#pragma once

#include <scheduler.h>
#include <mutex>

class RoundRobinScheduler : public IScheduler {
public:
    explicit RoundRobinScheduler(const std::vector<Resources>& clusterConfiguration)
      : IScheduler(clusterConfiguration) {
    }

    std::string GetName() const override {
        return "RoundRobin";
    }

protected:
    void TrySchedule(TTime time) override;
    void ReleaseTaskResources(TaskHolder* holder) override;

private:
    void TryScheduleImpl(TTime time, size_t begin, size_t end);

private:
    std::mutex mutex_;

    long double max_memory_ = 100;
    long double max_cpus_ = 100;
};