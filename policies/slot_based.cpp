#include <policies/slot_based.h>

#include <algorithm>
#include <unordered_set>
#include <iostream>

#include <utils.h>

int GetNumberOfSlots(const Resources& r, const Resources& slot_size) {
    if (r.cpus == 0 && r.memory == 0) {
        return 0;
    }
    return std::max((int)std::ceil(r.cpus / slot_size.cpus), (int)std::ceil(r.memory / slot_size.memory));
}

void SlotBasedScheduler::TrySchedule(TTime time) {
    std::priority_queue<std::pair<int, TUserId>,
            std::vector<std::pair<int, TUserId>>,
            std::greater<>> user_slots;

    for (const auto& [id, demand] : user_resources_) {
        user_slots.push({GetNumberOfSlots(demand.acquired, slot_size_), id});
    }

    std::unordered_map<TUserId, std::unordered_set<size_t>> user_tasks_idxs;
    for (size_t i = 0; i < scheduled_tasks_.size(); i++) {
        // Skip tasks from future
        if (scheduled_tasks_[i]->GetScheduledTime() > time) {
            continue;
        }
        user_tasks_idxs[scheduled_tasks_[i]->GetUserId()].insert(i);
    }

    while (!scheduled_tasks_.empty()) {
        while (!user_slots.empty() && user_tasks_idxs[user_slots.top().second].empty()) {
            user_slots.pop();
        }
        if (user_slots.empty()) {
            break;
        }
        auto user_id = user_slots.top().second;

        // Find a task that maximizes Dominant Resource Share for used_id
        auto task_id = *std::max_element(user_tasks_idxs[user_id].begin(), user_tasks_idxs[user_id].end(),
                                         [&](size_t lhs, size_t rhs) {
                                             return GetNumberOfSlots(scheduled_tasks_[lhs]->GetRequirements(), slot_size_)
                                                        < GetNumberOfSlots(scheduled_tasks_[rhs]->GetRequirements(), slot_size_);
                                         });

        auto taskHolder = scheduled_tasks_[task_id];
        int need_slots = GetNumberOfSlots(taskHolder->GetRequirements(), slot_size_);

        bool scheduled = false;
        for (size_t i = 0; i < clusterState_.size(); i++) {
            if (clusterState_[i].TryAcquire(slot_size_ * need_slots)) {
                taskHolder->Execute(time, static_cast<int64_t>(i));
                user_resources_[taskHolder->GetUserId()].acquired += slot_size_ * need_slots;
                finish_queue_.push({time + taskHolder->GetDuration(), taskHolder});
                stats_calcer_.Callback(this, time);

                scheduled_tasks_[task_id] = nullptr;
                user_slots.pop();
                user_slots.push({GetNumberOfSlots(user_resources_[user_id].acquired, slot_size_), user_id});
                user_tasks_idxs[user_id].erase(task_id);

                scheduled = true;
                break;
            }
        }
        if (!scheduled) {
            break;
        }
    }

    auto it = std::remove_if(scheduled_tasks_.begin(), scheduled_tasks_.end(), [](TaskHolder* item) {
        return item == nullptr;
    });
    scheduled_tasks_.erase(it, scheduled_tasks_.end());
}

void SlotBasedScheduler::ReleaseTaskResources(TaskHolder *holder) {
    int slots_num = GetNumberOfSlots(holder->GetRequirements(), slot_size_);
    Resources resources = slot_size_ * slots_num;

    clusterState_[holder->GetMachineId()].Release(resources);
    user_resources_[holder->GetUserId()].acquired -= resources;
    user_resources_[holder->GetUserId()].requested -= resources;
}