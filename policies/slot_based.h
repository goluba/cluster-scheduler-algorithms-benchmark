#pragma once

#include <scheduler.h>

const size_t SLOTS_PER_MACHINE = 20;

// Slot based min-max fairness algorithm
// https://www.usenix.org/legacy/event/nsdi11/tech/slides/ghodsi.pdf
class SlotBasedScheduler : public IScheduler {
public:
    explicit SlotBasedScheduler(const std::vector<Resources>& clusterConfiguration)
            : IScheduler(clusterConfiguration) {
        Resources mean_node{0, 0};
        for (const auto& x : clusterConfiguration) {
            mean_node += x;
        }
        // Calculate capacities of mean node in cluster
        mean_node /= clusterConfiguration.size();

        // Calculate slot size based on SLOTS_PER_MACHINE for mean node
        mean_node /= SLOTS_PER_MACHINE;

        slot_size_ = mean_node;
    }

    std::string GetName() const override {
        return "SlotBased";
    }

protected:
    void TrySchedule(TTime time) override;
    void ReleaseTaskResources(TaskHolder* holder) override;

private:
    Resources slot_size_;
};