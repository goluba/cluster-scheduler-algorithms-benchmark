#include <policies/srtf.h>
#include <algorithm>

void SRTFScheduler::TrySchedule(TTime time) {
    if (scheduled_tasks_.empty()) {
        return;
    }

    std::sort(scheduled_tasks_.begin(), scheduled_tasks_.end(), [](const TaskHolder* lhs, const TaskHolder* rhs){
        return lhs->GetDuration() < rhs->GetDuration();
    });

    std::deque<TaskHolder*> remaining_tasks;

    while (!scheduled_tasks_.empty()) {
        auto taskHolder = scheduled_tasks_.front();
        scheduled_tasks_.pop_front();

        // Skip tasks from future
        if (taskHolder->GetScheduledTime() > time) {
            remaining_tasks.push_back(taskHolder);
            continue;
        }

        bool scheduled = false;
        for (size_t i = 0; i < clusterState_.size(); i++) {
            if (clusterState_[i].TryAcquire(taskHolder->GetRequirements())) {
                ExecuteTask(taskHolder, time, i);
                scheduled = true;
                break;
            }
        }
        if (!scheduled) {
            remaining_tasks.push_back(taskHolder);
            break;
        }
    }
    std::copy(scheduled_tasks_.begin(), scheduled_tasks_.end(), std::back_inserter(remaining_tasks));
    std::swap(scheduled_tasks_, remaining_tasks);
}

void SRTFScheduler::ReleaseTaskResources(TaskHolder *holder) {
    auto resources = holder->GetRequirements();
    clusterState_[holder->GetMachineId()].Release(resources);
    user_resources_[holder->GetUserId()].acquired -= resources;
    user_resources_[holder->GetUserId()].requested -= resources;
}