#pragma once

#include <scheduler.h>

// Shortest Remaining Time First
// https://xia.cs.cmu.edu/resources/Documents/grandl_sigcomm14.pdf
class SRTFScheduler : public IScheduler {
public:
    explicit SRTFScheduler(const std::vector<Resources>& clusterConfiguration)
            : IScheduler(clusterConfiguration) {
    }

    std::string GetName() const override {
        return "SRTF";
    }

protected:
    void TrySchedule(TTime time) override;
    void ReleaseTaskResources(TaskHolder* holder) override;
};