#include <algorithm>
#include <iomanip>
#include <iostream>
#include <set>
#include <sstream>
#include <unordered_set>

#include <policies/tetris.h>
#include <utils.h>

long double CalcCompatibility(Resources lhs, Resources rhs) {
    return lhs.cpus * rhs.cpus + lhs.memory * rhs.memory;
}

void TetrisScheduler::TrySchedule(TTime time) {
    std::vector<std::set<std::pair<long double, size_t>>> task_node_compatibility(scheduled_tasks_.size());
    const auto& cluster_capacity = stats_calcer_.cluster_capacity;

    // Desc Sort by User Dominant Share
    auto fairness_cmp = [this, cluster_capacity](size_t lhs, size_t rhs) {
        auto l_user_id = scheduled_tasks_[lhs]->GetUserId();
        auto r_user_id = scheduled_tasks_[rhs]->GetUserId();

        auto l_share = GetUserDominantShare(scheduled_tasks_[lhs]->GetRequirements() + user_resources_[l_user_id].acquired, cluster_capacity);
        auto r_share = GetUserDominantShare(scheduled_tasks_[rhs]->GetRequirements() + user_resources_[r_user_id].acquired, cluster_capacity);

        if (l_share == r_share) {
            return lhs < rhs;
        }
        return l_share < r_share;

    };
    std::vector<size_t> tasks_ids;

    for (size_t i = 0; i < scheduled_tasks_.size(); i++) {
        // Skip tasks from future
        if (scheduled_tasks_[i]->GetScheduledTime() > time) {
            continue;
        }
        bool added = false;
        for (int j = 0; j < clusterState_.size(); j++) {
            if (clusterState_[j].TryFit(scheduled_tasks_[i]->GetRequirements())) {
                if (!added) {
                    tasks_ids.push_back(i);
                    added = true;
                }
                task_node_compatibility[i].insert({
                    CalcCompatibility(scheduled_tasks_[i]->GetRequirements(), clusterState_[j].GetIdle()),
                    j
                });
            }
        }
    }

    std::unordered_set<size_t> filter_task_ids;
    while (!tasks_ids.empty()) {
        // Filter and sort current task ids
        {
            auto it = std::remove_if(tasks_ids.begin(), tasks_ids.end(), [this, &filter_task_ids](size_t x) {
                return scheduled_tasks_[x] == nullptr || filter_task_ids.count(x) > 0;
            });
            tasks_ids.erase(it, tasks_ids.end());
            std::sort(tasks_ids.begin(), tasks_ids.end(), fairness_cmp);
        }
        if (tasks_ids.empty()) {
            break;
        }
        filter_task_ids.clear();

        size_t cnt = std::max(1UL, static_cast<size_t>((1.0 - FairnessFactor) * tasks_ids.size()));

        long double max_comp = -1;
        size_t task_id = -1;
        size_t node_id = -1;

        for (size_t i = 0; i < cnt; i++) {
            auto task_comp = task_node_compatibility[tasks_ids[i]].rbegin()->first;
            if (max_comp < task_comp) {
                max_comp = task_comp;
                task_id = tasks_ids[i];
                node_id = task_node_compatibility[task_id].rbegin()->second;
            }
        }

        if (max_comp == -1) {
            std::cout << "Best fit search failed!\n";
            break;
        }

        auto node_idle_old = clusterState_[node_id].GetIdle();

        const auto& taskHolder = scheduled_tasks_[task_id];
        if (clusterState_[node_id].TryAcquire(taskHolder->GetRequirements())) {

            ExecuteTask(taskHolder, time, node_id);

            filter_task_ids.insert(task_id);

            scheduled_tasks_[task_id] = nullptr;
            task_node_compatibility[task_id].clear();

            // recalculate compatibility for node_id
            for (size_t i = 0; i < task_node_compatibility.size(); i++) {
                if (scheduled_tasks_[i] != nullptr) {
                    auto& node = clusterState_[node_id];
                    auto& task = scheduled_tasks_[i];

                    if (!task_node_compatibility[i].empty()) {
                        task_node_compatibility[i].erase({
                            CalcCompatibility(task->GetRequirements(), node_idle_old),
                            node_id
                        });
                    }

                    if (node.TryFit(task->GetRequirements())) {
                        task_node_compatibility[i].insert({
                            CalcCompatibility(task->GetRequirements(), node.GetIdle()),
                            node_id
                        });
                    } else if (task_node_compatibility[i].empty() && scheduled_tasks_[i] != nullptr) {
                        filter_task_ids.insert(i);
                    }
                }
            }
        }
    }

    auto it = std::remove_if(scheduled_tasks_.begin(), scheduled_tasks_.end(), [](TaskHolder* item) {
        return item == nullptr;
    });
    scheduled_tasks_.erase(it, scheduled_tasks_.end());
}

void TetrisScheduler::ReleaseTaskResources(TaskHolder *holder) {
    auto resources = holder->GetRequirements();
    clusterState_[holder->GetMachineId()].Release(resources);
    user_resources_[holder->GetUserId()].acquired -= resources;
    user_resources_[holder->GetUserId()].requested -= resources;
}

std::string TetrisScheduler::GetName() const {
    std::stringstream factor;
    factor << std::fixed << std::setprecision(2) << FairnessFactor;
    return "Tetris[" + factor.str() + "]";
}