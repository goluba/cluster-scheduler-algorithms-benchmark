#pragma once

#include <scheduler.h>

// Tetris with Dominant Resource Fairness
// https://xia.cs.cmu.edu/resources/Documents/grandl_sigcomm14.pdf
class TetrisScheduler : public IScheduler {
public:
    explicit TetrisScheduler(long double fairnessFactor, const std::vector<Resources>& clusterConfiguration)
            : IScheduler(clusterConfiguration)
            , FairnessFactor(fairnessFactor) {
    }

    std::string GetName() const override;

protected:
    void TrySchedule(TTime time) override;
    void ReleaseTaskResources(TaskHolder* holder) override;

private:
    long double FairnessFactor;
};