#include <iostream>
#include <fstream>
#include <reader.h>

GoogleReader::GoogleReader(const std::string &filePath, size_t randomSeed) : rnd_(randomSeed) {
    std::fstream input(filePath, std::ios::in | std::ios::binary);
    if (!dataset_.ParseFromIstream(&input)) {
        throw std::runtime_error("Failed to load dataset from file: " + filePath);
    }
}

std::vector<Resources> GoogleReader::GetClusterConfiguration(size_t limit) {
    std::vector<Resources> result;

    for (const auto& machine : dataset_.cluster_configuration()) {
        result.push_back({machine.capacity().memory(), machine.capacity().cpus()});
    }
    size_t size = std::min(limit, (size_t)dataset_.cluster_configuration_size());
    std::shuffle(result.begin(), result.end(), rnd_);
    return {result.begin(), std::next(result.begin(), static_cast<long>(size))};
}

std::vector<ITask> GoogleReader::GetSortedTasks(size_t limit) {
    std::vector<ITask> result;
    result.reserve(dataset_.tasks().size());

    for (const auto& task : dataset_.tasks()) {
        result.push_back({
            task.user_id(),
            task.start_time(),
            task.duration(),
            {
                task.resource_request().memory(),
                task.resource_request().cpus()
            }
        });
    }

    size_t size = std::min(limit, (size_t)dataset_.tasks_size());
    std::shuffle(result.begin(), result.end(), rnd_);

    std::sort(result.begin(), std::next(result.begin(), static_cast<long>(size)), [](const ITask& a, const ITask& b) {
        return a.start_time < b.start_time;
    });
    return {result.begin(), std::next(result.begin(), static_cast<long>(size))};
}
