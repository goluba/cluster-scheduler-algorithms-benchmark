#pragma once

#include <vector>
#include <string>
#include <iostream>
#include <random>

#include <task.h>
#include <proto/dataset.pb.h>

class IReader {
public:
    virtual ~IReader() = default;
    virtual std::vector<Resources> GetClusterConfiguration(size_t limit = -1) = 0;
    virtual std::vector<ITask> GetSortedTasks(size_t limit = -1) = 0;
};

// Reader for google dataset
// See @/data/download_google_dataset.ipynb for details
class GoogleReader : public IReader {
public:
    explicit GoogleReader(const std::string& filePath, size_t randomSeed);
    std::vector<Resources> GetClusterConfiguration(size_t limit) override;
    std::vector<ITask> GetSortedTasks(size_t limit) override;
private:
    Dataset dataset_;
    std::mt19937 rnd_;

};