#include <scheduler.h>

IScheduler::IScheduler(const std::vector<Resources> &clusterConfiguration)
  : stats_calcer_(clusterConfiguration) {
    for (const auto& machine : clusterConfiguration) {
        clusterState_.emplace_back(machine);
    }
}

void IScheduler::Submit(TaskHolder *task, TTime time) {
    task->Schedule(time);
    scheduled_tasks_.push_back(task);
    user_resources_[task->GetUserId()].requested += task->GetRequirements();

    TryComplete(time);
    TrySchedule(time);

//    stats_calcer_.Callback(this, time);
}

void IScheduler::WaitIdle() {
    while (!finish_queue_.empty()) {
        auto curTime = finish_queue_.top().first;
        TryComplete(curTime);
        TrySchedule(curTime);
    }
}

size_t IScheduler::WaitSize() const {
    return scheduled_tasks_.size();
}

void IScheduler::TryComplete(TTime time) {
    while (!finish_queue_.empty() && finish_queue_.top().first <= time) {
        auto top = finish_queue_.top();
        auto task = top.second;

        ReleaseTaskResources(task);

        task->Finish(top.first);
        finish_queue_.pop();

        stats_calcer_.Callback(this, top.first);

        TrySchedule(top.first);
    }
}

void IScheduler::ExecuteTask(TaskHolder *task, TTime time, size_t machine_id) {
    task->Execute(time, static_cast<int64_t>(machine_id));
    user_resources_[task->GetUserId()].acquired += task->GetRequirements();
    finish_queue_.push({time + task->GetDuration(), task});

    stats_calcer_.Callback(this, time);
}

ClusterStatsCalcer IScheduler::GetStats() const {
    return stats_calcer_;
}