#pragma once

#include <task.h>
#include <vector>
#include <string>
#include <queue>
#include <functional>
#include <unordered_map>

#include <stats/cluster_stats_calcer.h>

#include <types.h>

class IScheduler {
public:
    IScheduler(const std::vector<Resources>& clusterConfiguration);
    virtual ~IScheduler() = default;

    void Submit(TaskHolder* task, TTime time);
    void WaitIdle();
    size_t WaitSize() const;

    ClusterStatsCalcer GetStats() const;

    virtual std::string GetName() const = 0;

protected:
    virtual void TrySchedule(TTime time) = 0;
    virtual void ReleaseTaskResources(TaskHolder *holder) = 0;

    void TryComplete(TTime time);
    void ExecuteTask(TaskHolder* task, TTime time, size_t machine_id);

protected:
    friend class ClusterStatsCalcer;

    ClusterState clusterState_;
    std::deque<TaskHolder*> scheduled_tasks_;
    std::unordered_map<TUserId, UserResources> user_resources_;

    using Item = std::pair<TTime, TaskHolder*>;
    std::priority_queue<Item, std::vector<Item>, std::greater<>> finish_queue_;

    ClusterStatsCalcer stats_calcer_;
};