#include <stats/cluster_stats_calcer.h>
#include <scheduler.h>
#include <utils.h>

#include <iostream>

Resources GetUtilization(const Resources& clusterCapacity,
                         const std::unordered_map<TUserId, UserResources>& userResources) {
    Resources used{0, 0};
    for (const auto& [user, res]: userResources) {
        used += res.acquired;
    }
    return Resources{used.memory / clusterCapacity.memory, used.cpus / clusterCapacity.cpus};
}

Resources GetOverallocation(const std::unordered_map<TUserId, UserResources>& userResources) {
    Resources result{0, 0};
    for (const auto& [user, resources]: userResources) {
        auto over = resources.acquired - resources.requested;
        over.memory = std::max(over.memory, static_cast<long double>(0));
        over.cpus = std::max(over.cpus, static_cast<long double>(0));
        result += over;
    }
    return result;
}

ClusterStatsCalcer::ClusterStatsCalcer(const std::vector<Resources> &clusterConfiguration)
  : cluster_capacity({0, 0}){
    for (const auto& machine: clusterConfiguration) {
        cluster_capacity += machine;
    }
    assert(cluster_capacity.memory > EPS);
    assert(cluster_capacity.cpus > EPS);
}

void ClusterStatsCalcer::Callback(IScheduler *scheduler, TTime time) {
    // Resources Utilization
    Resources utilization = GetUtilization(cluster_capacity, scheduler->user_resources_);
    cpu_utilization[time] = utilization.cpus;
    memory_utilization[time] = utilization.memory;

    // Resources Overallocation
    overallocation[time] = GetOverallocation(scheduler->user_resources_);

    // Dominant Resource User Satisfaction Standard deviation
    {
        std::vector<long double> data;
        data.reserve(scheduler->user_resources_.size());
        for (const auto&[userId, resources]: scheduler->user_resources_) {
            long double satisfaction = std::max(CalcSatisfaction(resources.acquired.memory, resources.requested.memory),
                                                CalcSatisfaction(resources.acquired.cpus, resources.requested.cpus));
            data.push_back(satisfaction);
        }
        dominant_resource_user_satisfaction_deviation[time] = GetStandardDeviation(data);
    }

    // Dominant Resource Usage Standard Deviation
    {
        std::vector<long double> data;
        data.reserve(scheduler->user_resources_.size());
        for (const auto&[userId, resources]: scheduler->user_resources_) {
            long double dominant_usage = std::max(resources.acquired.cpus / cluster_capacity.cpus,
                                         resources.acquired.memory / cluster_capacity.memory);
            data.push_back(dominant_usage);
        }
        dominant_resource_usage_deviation[time] = GetStandardDeviation(data);
    }

}