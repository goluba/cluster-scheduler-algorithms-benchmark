#pragma once

#include <types.h>
#include <task.h>
#include <unordered_map>
#include <map>
#include <deque>

class IScheduler;

class ClusterStatsCalcer {
public:
    ClusterStatsCalcer(const std::vector<Resources> &clusterConfiguration);
    void Callback(IScheduler *scheduler, TTime time);

public:
    Resources cluster_capacity;

    std::map<TTime, long double> cpu_utilization;
    std::map<TTime, long double> memory_utilization;

    std::map<TTime, Resources> overallocation;
    std::map<TTime, long double> dominant_resource_user_satisfaction_deviation;
    std::map<TTime, long double> dominant_resource_usage_deviation;
};