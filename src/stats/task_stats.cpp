#include <stats/task_stats.h>

void TaskStats::Schedule(TTime time) {
    scheduledTime_ = time;
}

void TaskStats::Execute(TTime time) {
    waitTime_ = time - scheduledTime_;
}

void TaskStats::Finish(TTime /*time*/) {
    isFinished_ = true;
}

TTime TaskStats::GetWaitTime() const {
    return waitTime_;
}

TTime TaskStats::GetScheduledTime() const {
    return scheduledTime_;
}

bool TaskStats::IsExecuted() const {
    return isFinished_;
}