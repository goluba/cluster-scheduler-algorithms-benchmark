#pragma once

#include <cstdint>
#include <types.h>

class TaskStats {
public:
    void Schedule(TTime time);
    void Execute(TTime time);
    void Finish(TTime time);

    TTime GetWaitTime() const;
    TTime GetScheduledTime() const;
    bool IsExecuted() const;

private:
    TTime waitTime_{0};
    TTime scheduledTime_{0};
    bool isFinished_{false};
};