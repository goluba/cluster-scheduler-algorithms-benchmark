#include "task.h"

TaskHolder::TaskHolder(const ITask& task)
  : task_(task) {
}

TTime TaskHolder::GetDuration() const {
    return task_.duration;
}

Resources TaskHolder::GetRequirements() const {
    return task_.requested_resources;
}

TaskStats TaskHolder::GetStats() const {
    return stats_;
}

int64_t TaskHolder::GetMachineId() const {
    return machine_id_;
}

int64_t TaskHolder::GetUserId() const {
    return task_.user_id;
}

TTime TaskHolder::GetScheduledTime() const {
    return scheduled_time_;
}

void TaskHolder::Execute(TTime time, int64_t machineId) {
    stats_.Execute(time);
    machine_id_ = machineId;
}

void TaskHolder::Schedule(TTime time) {
    scheduled_time_ = time;
    stats_.Schedule(time);
}

void TaskHolder::Finish(TTime time) {
    stats_.Finish(time);
    machine_id_ = -1;
}