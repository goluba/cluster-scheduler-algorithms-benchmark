#pragma once

#include <cstddef>
#include <cstdint>
#include <stats/task_stats.h>
#include <types.h>

// Task from dataset
struct ITask {
    int64_t user_id;
    int64_t start_time;
    int64_t duration;
    Resources requested_resources;
};

// Used to maintain information and statistic about task execution
class TaskHolder {
public:
    explicit TaskHolder(const ITask& task);

    void Schedule(TTime time);
    void Execute(TTime time, TTime machineId);
    void Finish(TTime time);

    TTime GetDuration() const;
    Resources GetRequirements() const;
    int64_t GetMachineId() const;
    TTime GetScheduledTime() const;
    int64_t GetUserId() const;

    TaskStats GetStats() const;

private:
    ITask task_;

    int64_t machine_id_{-1};
    TTime scheduled_time_{-1};

    TaskStats stats_;
};
