#pragma once

#include <cstdint>
#include <vector>

using TTime = int64_t;
using TUserId = int64_t;

struct Resources {
    long double memory;
    long double cpus;

    Resources& operator+=(const Resources& rhs) {
        memory += rhs.memory;
        cpus += rhs.cpus;
        return *this;
    };

    Resources& operator-=(const Resources& rhs) {
        memory -= rhs.memory;
        cpus -= rhs.cpus;
        return *this;
    };

    Resources& operator*=(long double x) {
        memory *= x;
        cpus *= x;
        return *this;
    }

    Resources& operator/=(long double x) {
        memory /= x;
        cpus /= x;
        return *this;
    }

    friend Resources operator+(Resources lhs, const Resources& rhs) {
        lhs += rhs;
        return lhs;
    }

    friend Resources operator-(Resources lhs, const Resources& rhs) {
        lhs -= rhs;
        return lhs;
    }

    friend Resources operator*(Resources lhs, long double x) {
        lhs *= x;
        return lhs;
    }

    friend Resources operator/(Resources lhs, long double x) {
        lhs /= x;
        return lhs;
    }
};

struct ClusterNode {
public:
    explicit ClusterNode(Resources capacity) : capacity_(capacity), idle_(capacity) {
    }

    bool TryAcquire(Resources request) {
        if (idle_.cpus >= request.cpus && idle_.memory >= request.memory) {
            idle_ -= request;
            return true;
        }
        return false;
    }

    bool TryFit(Resources request) const {
        if (idle_.cpus >= request.cpus && idle_.memory >= request.memory) {
            return true;
        }
        return false;
    }

    void Release(Resources resources) {
        idle_ += resources;
    }

    Resources GetIdle() const {
        return idle_;
    }

    Resources GetCapacity() const {
        return capacity_;
    }

private:
    const Resources capacity_;
    Resources idle_;
};

struct UserResources {
    Resources requested;
    Resources acquired;
};

using ClusterState = std::vector<ClusterNode>;
