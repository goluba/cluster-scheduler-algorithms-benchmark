#pragma once

#include <cmath>
#include <numeric>

#include <types.h>

const long double EPS = 1e-9;

template <class T>
T sqr(T x) {
    return x * x;
}

inline long double CalcSatisfaction(long double acquired, long double requested) {
    if (requested < EPS) {
        return 1;
    }
    return acquired / requested;
}

template <class T>
long double GetStandardDeviation(const std::vector<T>& data) {
    long double mean = std::accumulate(data.begin(), data.end(), static_cast<long double>(0)) / static_cast<long double>(data.size());
    long double deviation = 0;
    for (const auto& x : data) {
        deviation += sqr(x - mean);
    }
    deviation /= static_cast<long double>(data.size());
    return std::sqrtl(deviation);
}

inline long double GetUserDominantShare(const Resources& user_demand, const Resources& cluster_capacity) {
    auto mem = user_demand.memory / cluster_capacity.memory;
    auto cpu = user_demand.cpus / cluster_capacity.cpus;
    return std::max(mem, cpu);
}
